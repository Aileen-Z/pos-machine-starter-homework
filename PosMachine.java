package pos.machine;

import java.util.*;

public class PosMachine {

    public String printReceipt(List<String> barcodes) {
        List<Item> database  = ItemsLoader.loadAllItems();
        Map<String, Integer> quatity = calculateItemsQuantity(barcodes);
        List<String> items = calEachTotalandGenLine(database, quatity);
        int total = calculateTotal(items);
        String receipt = generateReceipt(items, total);
        return receipt;

    }

    private Map<String, Integer> calculateItemsQuantity(List<String> barcodes){
        Map<String, Integer> quantity = new LinkedHashMap<>();
        //https://blog.csdn.net/Mcdull__/article/details/114358626
        //https://blog.csdn.net/xiaxingyi/article/details/109328365
        for(int i = 0; i < barcodes.size(); i++){
            quantity.put(barcodes.get(i), quantity.getOrDefault(barcodes.get(i), 0) +1);
        }
        return quantity;
    }

    private List<String> calEachTotalandGenLine(List<Item> database, Map<String, Integer> quantity){
        List<String> items = new ArrayList<>();
        for(String barcode : quantity.keySet()){
            for(Item item: database){
                if(item.getBarcode().equals(barcode)){
                    int num = quantity.get(barcode);
                    int unitprice = item.getPrice();
                    int eachTotal = unitprice * num;
                    generateitemLine(items, item, num, eachTotal);
                    break;
                }
            }
        }
        return items;

    }

    private void generateitemLine(List<String> items, Item item, int num, int eachTotal){
        items.add(
                String.format(
                        "Name: %s, Quantity: %d, Unit price: %d (yuan), Subtotal: %d (yuan)",
                        item.getName(), num, item.getPrice(), eachTotal
                )
        );
    }

    private int calculateTotal(List<String> items){
        int total = 0;
        for(String item : items){
            String[] each = item.split(":");
            String subtotal = each[each.length-1].trim().split(" ")[0];
            total += Integer.parseInt(subtotal);

        }
        return total;
    }

    private String generateReceipt(List<String> items, int total){
        String receipt = "***<store earning no money>Receipt***\n" +
                String.join("\n", items) + "\n" +
                "----------------------\n" +
                String.format("Total: %d (yuan)\n", total) +
                "**********************";
        return receipt;
    }

}


